#sg
#generates the abstractness score feature vector
def makedict(filename):
    f = open(filename, 'r')
    score = {}
    for line in f:
        lw = line.rstrip('\n').split(' ')
        score[lw[0]] = float(lw[1].rstrip('\n'))
    return score


def abscore(sentence, score_dict):
    func_words = ['is', 'was', 'a', 'to' , 'her', 'the', 'in', 'his', 'i', 'that', 'were', 'of', 'my', 'are', 'did', 'not', 'want', 'on', 'had', 'for', 'an', 'from', 'and']
    res = []
    l = []
    words = sentence.rstrip('\n').split(' ')
    for w in words:
        if(w in func_words):
            continue
        w = w.rstrip(' \n')
        try:
            l.append((w, score_dict[w]))
        except:
            continue
            pass
    for ll in  l:
         res.append(ll[1])
    if(len(l) < 5):
        for i in range(0, 5 - len(l)):
             res.append(0.5)
    return l

import sys
if __name__ == '__main__':
    dict = makedict('abscore.txt')
    print abscore(sys.argv[1], dict)
