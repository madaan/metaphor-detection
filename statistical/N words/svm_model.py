#sg
import numpy as np
from sklearn import svm, grid_search, datasets
from numpy import genfromtxt, savetxt

def train():
    dataset = genfromtxt(open('train.txt','r'), delimiter=' ', dtype='f8')[1:]    
    target = [x[5] for x in dataset]
    train = [x[0:5] for x in dataset]
    '''
    parameters = {'kernel':['rbf'], 'C':[2**i for i in range(-3,11)],  'gamma':[2**i for i in range(-3,11)]} 

    svr = svm.SVC(probability = True)
    
    #train with cross validation
    clf = grid_search.GridSearchCV(svr, parameters)
    '''
    clf = svm.SVC()
    clf.fit(train, target)
    return clf
 
def makedict(filename):
    f = open(filename, 'r')
    score = {}
    for line in f:
        lw = line.rstrip('\n').split(' ')
        score[lw[0]] = float(lw[1].rstrip('\n'))
    return score


def abscore(sentence, score_dict):
    func_words = ['is', 'was', 'a', 'to' , 'her', 'the', 'in', 'his', 'i', 'that', 'were', 'of', 'my', 'are', 'did', 'not', 'want', 'on', 'had', 'for', 'an', 'from', 'and']
    res = []
    l = []
    words = sentence.rstrip('\n').split(' ')
    for w in words:
        if(w in func_words):
            continue
        w = w.rstrip(' \n')
        try:
            l.append((w, score_dict[w]))
        except:
            continue
            pass
    for ll in  l:
         res.append(ll[1])
    if(len(l) < 5):
        for i in range(0, 5 - len(l)):
             res.append(0.5)
    return res





if __name__ == '__main__':
    clf = train()
    print clf
    dict = makedict('abscore.txt')
    f = open('testdata.txt', 'r')
    ip = ''
    '''
    while(ip != 'xx'):
        ip = raw_input('> ')
        print clf.predict(abscore(ip, dict))
    '''

    for i, line in enumerate(f):
        print i,
        if(int(clf.predict(abscore(line, dict))[0]) == 1):
            print 'Metaphor'
        else:
            print 'Non Metaphor'

