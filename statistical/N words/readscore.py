#sg
#This file reads up the abslist and finds out abstractness score of the words
def makedict(filename):
    f = open(filename, 'r')
    score = {}
    for line in f:
        lw = line.rstrip('\n').split(' ')
        score[lw[0]] = float(lw[1].rstrip('\n'))
    return score

def readscore(example_filename, score):
    func_words = ['is', 'was', 'a', 'to' , 'her', 'the', 'in', 'his', 'i', 'that', 'were', 'of', 'my', 'are', 'did', 'not', 'want', 'on', 'had', 'for', 'an', 'from', 'and']
    ef = open(example_filename, 'r')
    for line in ef:
        l = []
        words = line.rstrip('\n').split(' ')
        for w in words:
            if(w in func_words):
                continue
            w = w.rstrip(' \n')
            try:
                l.append((w, score[w]))
            except:
                continue
                pass
            

       
        for ll in  l:
            print ll[1],
        if(len(l) < 5):
            for i in range(0, 5 - len(l)):
                print '0.5',
        print '1'
        

if __name__ == '__main__':
    d = makedict('abscore.txt')
    
    readscore('metlist', d)
    
