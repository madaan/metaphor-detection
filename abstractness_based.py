#sg

from nltk import pos_tag, word_tokenize
import re
from nltk.corpus import wordnet as wn
from abstract import abstract_score

def main(sentence):
    #import the corpus to calculate similarity
    from nltk.corpus import wordnet_ic
    #find out the nouns
    sentence_l = [sent.lower() for sent in sentence.split(' ')]
    tagged_sentence = pos_tag(word_tokenize(str(' '.join(sentence_l))))
    

    #print tagged_sentence
    nouns = [pair for pair in tagged_sentence  if (re.search('^NN?', pair[1]) != None)]


    #print nouns

    MET_THRESH = .05
    found = False
    for i in range(0, len(nouns) - 1):

        ascore_l = abstract_score(nouns[i][0])
           
        ascore_r = abstract_score(nouns[i + 1][0])
        if(abs(ascore_r - ascore_l) > MET_THRESH):
            #print 'Umm, \'%s - %s\' seems like metaphorical to me.' % (word_l, word_r)
            found = True
            return 1
        else:
            return 0

    if(found == False):
        #print 'I think your sentence is clean'
        return 0

if __name__ == '__main__':
    ip = ''
    while(ip != 'x'):
        ip = raw_input('Enter the sentence : ')
        res = main(ip)
        output = 'Metaphor' if res == 1 else 'Clean'
        print output
    '''
    training_file = open('met_list', 'r')
    result_list = []
    c = 0
    import os
    for i, line in enumerate(training_file):
        os.system('clear')
        print '%d / 70' % i
        res = main(line.split(',')[0])
        truel = int(line.split(',')[1])
        if(res == truel):
            result_list.append(1)
        else:
            result_list.append(0)
     

    print 'Accuracy : %f' %(1.0 * sum(result_list) / len(result_list))
    '''
