A heart of gold
       [('A', 'DT'), ('heart', 'NN'), ('of', 'IN'), ('gold', 'NN')]
Heart of a lion
       [('Heart', 'NN'), ('of', 'IN'), ('a', 'DT'), ('lion', 'NN')]
Ideas are water
       [('Ideas', 'NNS'), ('are', 'VBP'), ('water', 'NN')]
Apple of the eye
       [('Apple', 'NNP'), ('of', 'IN'), ('the', 'DT'), ('eye', 'NN')]
He is the sun of my sky
       [('He', 'PRP'), ('is', 'VBZ'), ('the', 'DT'), ('sun', 'NN'), ('of', 'IN'), ('my', 'PRP$'), ('sky', 'NN')]
It's raining cats and dogs
       [('It', 'PRP'), ("'s", 'VBZ'), ('raining', 'VBG'), ('cats', 'NNS'), ('and', 'CC'), ('dogs', 'NNS')]
Her hair was bone white
       [('Her', 'PRP$'), ('hair', 'NN'), ('was', 'VBD'), ('bone', 'NN'), ('white', 'RB')]
He has a heart of stone.
       [('He', 'PRP'), ('has', 'VBZ'), ('a', 'DT'), ('heart', 'NN'), ('of', 'IN'), ('stone', 'NN'), ('.', '.')]
time is money
       [('time', 'NN'), ('is', 'VBZ'), ('money', 'NN')]
blanket of snow
       [('blanket', 'NN'), ('of', 'IN'), ('snow', 'NN')]
monkey mind
       [('monkey', 'NN'), ('mind', 'VBD')]
Bells and Whistles
       [('Bells', 'NNS'), ('and', 'CC'), ('Whistles', 'NNP')]
To Have a Bee in Your Bonnet
       [('To', 'TO'), ('Have', 'VB'), ('a', 'DT'), ('Bee', 'NNP'), ('in', 'IN'), ('Your', 'PRP$'), ('Bonnet', 'NNP')]
Butterflies in Your Stomach
       [('Butterflies', 'NNS'), ('in', 'IN'), ('Your', 'PRP$'), ('Stomach', 'NNP')]
Open a Can of Worms
       [('Open', 'NNP'), ('a', 'DT'), ('Can', 'NNP'), ('of', 'IN'), ('Worms', 'NNP')]
A Fish Out of Water
       [('A', 'DT'), ('Fish', 'JJ'), ('Out', 'IN'), ('of', 'IN'), ('Water', 'NNP')]
I have butter fingers today
       [('I', 'PRP'), ('have', 'VBP'), ('butter', 'JJR'), ('fingers', 'NNS'), ('today', 'NN')]
My father is a rock
       [('My', 'PRP$'), ('father', 'NN'), ('is', 'VBZ'), ('a', 'DT'), ('rock', 'NN')]
Love is a rose
       [('Love', 'NNP'), ('is', 'VBZ'), ('a', 'DT'), ('rose', 'NN')]
Love a fine wine
       [('Love', 'NNP'), ('a', 'DT'), ('fine', 'NN'), ('wine', 'NN')]
Love a garden
       [('Love', 'NNP'), ('a', 'DT'), ('garden', 'NN')]
Love a journey
       [('Love', 'NNP'), ('a', 'DT'), ('journey', 'NN')]
Love an ocean
       [('Love', 'NNP'), ('an', 'DT'), ('ocean', 'JJ')]
sea of ghosts
       [('sea', 'NN'), ('of', 'IN'), ('ghosts', 'NNS')]
sea of bee
       [('sea', 'NN'), ('of', 'IN'), ('bee', 'NN')]
sea of love
       [('sea', 'NN'), ('of', 'IN'), ('love', 'NN')]
sea of umbrellas
       [('sea', 'NN'), ('of', 'IN'), ('umbrellas', 'NNS')]
Bell the cat
       [('Bell', 'NNP'), ('the', 'DT'), ('cat', 'NN')]
blanket of bullet
       [('blanket', 'NN'), ('of', 'IN'), ('bullet', 'NN')]
blanket of clouds
       [('blanket', 'NN'), ('of', 'IN'), ('clouds', 'NNS')]
blanket of flowers
       [('blanket', 'NN'), ('of', 'IN'), ('flowers', 'NNS')]
blanket of ghosts
       [('blanket', 'NN'), ('of', 'IN'), ('ghosts', 'NNS')]
blanket of love
       [('blanket', 'NN'), ('of', 'IN'), ('love', 'NN')]
blanket of roses
       [('blanket', 'NN'), ('of', 'IN'), ('roses', 'NNS')]
blanket of stars
       [('blanket', 'NN'), ('of', 'IN'), ('stars', 'NNS')]
couch patato
       [('couch', 'JJ'), ('patato', 'NN')]
eyes were fireflies
       [('eyes', 'NNS'), ('were', 'VBD'), ('fireflies', 'NNS')]
eyes were sauccers
       [('eyes', 'NNS'), ('were', 'VBD'), ('sauccers', 'NNS')]
hand over feet
       [('hand', 'NN'), ('over', 'IN'), ('feet', 'NN')]
home was prison
       [('home', 'NN'), ('was', 'VBD'), ('prison', 'NN')]
puppet government
       [('puppet', 'NN'), ('government', 'NN')]
sea of fire
       [('sea', 'NN'), ('of', 'IN'), ('fire', 'NN')]
sea of knowledge
       [('sea', 'NN'), ('of', 'IN'), ('knowledge', 'NN')]
snake oil
       [('snake', 'NN'), ('oil', 'NN')]
Truth is food
       [('Truth', 'NNP'), ('is', 'VBZ'), ('food', 'NN')]
You are the light in my life 
       [('You', 'PRP'), ('are', 'VBP'), ('the', 'DT'), ('light', 'JJ'), ('in', 'IN'), ('my', 'PRP$'), ('life', 'NN')]
Cold heart
       [('Cold', 'NNP'), ('heart', 'NN')]
ice cold
       [('ice', 'NN'), ('cold', 'VBD')]

       []

       []

       []
