#sg

def abstract_score(word):

    MAX_DEPTH = 15

    '''Attempts to return the abstractness measure of a given word 
    by finding out length of the hypernymy tree'''

    #Assumtion : The first hypernym is given higher precedence always
    #first recursively list all the hypernyms and see if this works

    from nltk.corpus import wordnet as wn

    '''
    Uncomment to print ancestors along the leftmost branch recursively
    ancestors = []
    hyprnms = wn.synset(wn.synsets(word, wn.NOUN)[0].name).hypernyms()

    while(len(hyprnms) > 0):
        print hyprnms[0]
        ancestors.append(hyprnms[0])
        hyprnms = hyprnms[0].hypernyms()
    '''
    
    lengths = []
    for synst in  wn.synsets(word, wn.NOUN):
        try:
            lengths.append(wn.synset(synst.name).max_depth())
        except AttributeError:
            print 'not def'
    try:
        c_score = max(lengths)
    except ValueError:
        c_score = 2*MAX_DEPTH
    a_score = (1.0 * MAX_DEPTH - c_score) / (MAX_DEPTH)
    return a_score


import sys
if __name__ == '__main__':
    for av in sys.argv[1:]:
        print '%s : %5.4f' % (av,abstract_score(av))
