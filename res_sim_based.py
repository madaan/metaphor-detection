#sg

from nltk import pos_tag, word_tokenize
import re
from nltk.corpus import wordnet as wn

def main(sentence):
    #import the corpus to calculate similarity
    from nltk.corpus import wordnet_ic
    brown_ic = wordnet_ic.ic('ic-brown.dat')
    #find out the nouns
    sentence_l = [sent.lower() for sent in sentence.split(' ')]
    tagged_sentence = pos_tag(word_tokenize(str(' '.join(sentence_l))))
    

    #print tagged_sentence
    nouns = [pair for pair in tagged_sentence  if (re.search('^NN?', pair[1]) != None)]

    found = False
    for i in range(0, len(nouns) - 1):

        try:
            word_l = nouns[i][0]
            synset_l = wn.synset(wn.synsets(word_l, wn.NOUN)[0].name)
            word_r = nouns[i + 1][0]
            synset_r = wn.synset(wn.synsets(word_r, wn.NOUN)[0].name)
        except IndexError:
            continue
        
        
        print '\n\t', word_l,' : ', synset_l.definition,'\n\n\t', word_r,' : ', synset_r.definition,'\n'

        ic_similarity = synset_l.res_similarity(synset_r, brown_ic)
        print '\n\tLowest common hypernym : ', synset_l.lowest_common_hypernyms(synset_r)
        print '\n\tic_similarity : ', ic_similarity

        if(ic_similarity < 2):
            print '\n\tUmm, \'%s - %s\' seems like metaphorical to me.' % (word_l, word_r)
            found = True
            return 1
        else:
            return 0

    if(found == False):
        #print 'I think your sentence is clean'
        return 0


if __name__ == '__main__':
    ip = ''
    while(ip != 'x'):
        ip = raw_input('Enter the sentence : ')
        import os
        os.system('clear')
        print '\t\tTesting  "', ip,'"\n\n'
        res = main(ip)
        output = 'Metaphor' if res == 1 else 'Clean'
        print output
    '''
    training_file = open('met_list', 'r')
    result_list = []
    c = 0
    import os
    for i, line in enumerate(training_file):
        os.system('clear')
        print '%d / 70' % i
        res = main(line.split(',')[0])
        truel = int(line.split(',')[1])
        if(res == truel):
            result_list.append(1)
        else:
            result_list.append(0)
     

    print 'Accuracy : %f' %(1.0 * sum(result_list) / len(result_list))
    '''


